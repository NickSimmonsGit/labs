#include <stdint.h>	
#include <stdlib.h> 
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "motor.h"

static GPIO_InitTypeDef GPIOA_InitStruct;
static GPIO_InitTypeDef GPIOC_InitStruct;
static TIM_HandleTypeDef htim3;
static TIM_OC_InitTypeDef sConfig_TIM;
static int32_t enc_count = 0;

void motor_PWM_init(void)
{
	/* TODO: Enable TIM3 clock */
	__TIM3_CLK_ENABLE();
	
	/* Enable GPIOA clock */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	

	/* Initialise PA6 with:
	- Pin 6
	- Alternate function push-pull mode
	- No pull
	- High frequency
	- Alternate function 2 - Timer 3*/
	GPIOA_InitStruct.Pin = GPIO_PIN_6;
	GPIOA_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIOA_InitStruct.Pull = GPIO_NOPULL;
	GPIOA_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIOA_InitStruct.Alternate = GPIO_AF2_TIM3;
	HAL_GPIO_Init(GPIOA, &GPIOA_InitStruct);
	

	/* Initialise timer 3 with:
	- Instance TIM3
	- Prescaler of 1
	- Counter mode up
	- Timer period to generate a 10kHz signal
	- Clock division of 0 */
	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 1;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 10000;
	htim3.Init.ClockDivision = 0;
	HAL_TIM_PWM_Init(&htim3);
	

	/* Configure timer 3, channel 1 with:
	- Output compare mode PWM1
	- Pulse = 0
	- OC polarity high
	- Fast mode disabled */
	sConfig_TIM.OCMode = TIM_OCMODE_PWM1;
	sConfig_TIM.Pulse = 0;
	sConfig_TIM.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfig_TIM.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_TIM_PWM_ConfigChannel(&htim3, &sConfig_TIM, TIM_CHANNEL_1);
	

	/* Set initial Timer 3, channel 1 compare value */
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 2500);
	
	/* Start Timer 3, channel 1 */
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
}

void motor_encoder_init(void)
{
	/* Enable GPIOC clock */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	/*Initialise PC0, PC1 with:
	- Pin 0|1
	- Interrupt rising and falling edge
	- No pull
	- High frequency */
	GPIOC_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1;
	GPIOC_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIOC_InitStruct.Pull = GPIO_NOPULL;
	GPIOC_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIOC_InitStruct);

	/* Set priority of external interrupt lines 0,1 to 0x0f, 0x0f
	To find the IRQn_Type definition see "MCHA3500 Windows Toolchain\workspace\STM32Cube_F4_FW\Drivers\
	CMSIS\Device\ST\STM32F4xx\Include\stm32f446xx.h" */
	HAL_NVIC_SetPriority(6, 0x0f, 0x0f);
	HAL_NVIC_SetPriority(7, 0x0f, 0x0f);
	
	/* Enable external interrupt for lines 0, 1 */
	HAL_NVIC_EnableIRQ(6);
	HAL_NVIC_EnableIRQ(7);
}

void EXTI0_IRQHandler(void)
{
	/* Check if PC0 == PC1. Adjust encoder count accordingly. */
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1)) {
		enc_count++;
	}
	else {
		enc_count--;
	}

	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
	/* Check if PC0 == PC1. Adjust encoder count accordingly. */
	
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1)) {
		enc_count--;
	}
	else {
		enc_count++;
	}
	
	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

int32_t motor_encoder_getValue(void)
{
	return enc_count;
}









