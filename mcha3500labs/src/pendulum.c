#include <stdint.h>	
#include <stdlib.h> 
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"

static GPIO_InitTypeDef GPIOB_InitStruct;
static ADC_HandleTypeDef ADC1_InitStruct;
static ADC_ChannelConfTypeDef sConfig1;
static float pendPolling;
static float pendVoltage;

void pendulum_init(void)
{
 /* Enable ADC1 clock */
  __HAL_RCC_ADC1_CLK_ENABLE();
 
 /* Enable GPIOB clock */
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
 /* Initialise PB0 with:
	- Pin 0
	- Analog mode
	- No pull
	- High frequency */
	GPIOB_InitStruct.Pin = GPIO_PIN_0;
	GPIOB_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIOB_InitStruct.Pull = GPIO_NOPULL;
	GPIOB_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIOB_InitStruct);

/* Initialise ADC1 with:
- Instance ADC 1
	- Div 2 prescaler
	- 12 bit resolution
	- Data align right
	- Continuous conversion mode disabled
	- Number of conversions = 1
	Note: Configuration parameters not mentioned above
	are not required for this lab. */
	ADC1_InitStruct.Instance = ADC1;
	ADC1_InitStruct.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
	ADC1_InitStruct.Init.Resolution = ADC_RESOLUTION_12B;
	ADC1_InitStruct.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	ADC1_InitStruct.Init.ContinuousConvMode = DISABLE;
	ADC1_InitStruct.Init.NbrOfConversion = 1;
	HAL_ADC_Init(&ADC1_InitStruct);

/* Configure ADC channel to:
	- Channel 8
	- Rank 1
	- Sampling time 480 cycles
	- Offset 0 */
	sConfig1.Channel = ADC_CHANNEL_8;
	sConfig1.Rank = 1;
	sConfig1.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	sConfig1.Offset = 0;
	HAL_ADC_ConfigChannel(&ADC1_InitStruct, &sConfig1);
}


float pendulum_read_voltage(void)
{
	/* Start ADC */
	HAL_ADC_Start(&ADC1_InitStruct);
	
	/* Poll for conversion. Use timeout of 0xFF. */
	HAL_ADC_PollForConversion(&ADC1_InitStruct, 0xFF);
	
	/* Get ADC value */
	pendPolling = HAL_ADC_GetValue(&ADC1_InitStruct);
	
	/* Stop ADC */
	HAL_ADC_Stop(&ADC1_InitStruct);

	/* Compute voltage from ADC reading. Hint: 2^12-1 = 4095 */
	pendVoltage = (pendPolling/4095.0)*3.3;

	/* Return the computed voltage */
	return pendVoltage;
}







