#include <stdint.h>	
#include <stdlib.h> 
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "data_logging.h"

/* Variable declarations */
uint16_t logCount;
static void (*log_function)(void);
static osTimerId_t log_Timer_Id;
static osTimerAttr_t logTimerAttr = {
    .name = "log_timer" 
};

/* Function declarations */
static void log_pendulum(void);
static void logging_stop(void);	
static void log_pointer(void *argument);

/* Function defintions */

void logging_init(void)
{
	/* Initialise timer for use with pendulum data logging */
	log_Timer_Id = osTimerNew(log_pointer, osTimerPeriodic, NULL, &logTimerAttr);
}

static void log_pointer(void *argument)
{
	UNUSED(argument);

	/* Call function pointed to by log_function */
	(*log_function)();
}

static void log_pendulum(void)
{
	float timerVal = (float)logCount*0.005;
	/* Read the potentiometer voltage */
	float potVoltage = pendulum_read_voltage();
	/* Print the sample time and potentiometer voltage to the serial terminal in the format [time],[
	voltage] */
	printf("%f,%f\n", timerVal, potVoltage);
	/* Increment log count */
	logCount = logCount + 1;
	/* Stop logging once 2 seconds is reached (Complete this once you have created the stop function
	in the next step) */
	if(logCount == 400)
	{
		logging_stop();
	}
	
}

void pend_logging_start(void)
{
	/* Change function pointer to the pendulum logging function */
	log_function = &log_pendulum;
	/* Reset the log counter */
	logCount = 0;
	/* Start data logging timer at 200Hz */
	osTimerStart(log_Timer_Id, 5);
}

void logging_stop(void)
{
	/* Stop data logging timer */
	osTimerStop(log_Timer_Id);
}







